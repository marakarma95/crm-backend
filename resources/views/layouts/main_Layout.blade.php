<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CRM | @yield('title')</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" href="{{ asset('template/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{ asset('template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{ asset('template/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('template/plugins/jqvmap/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('template/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{ asset('template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{ asset('template/plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{ asset('template/plugins/summernote/summernote-bs4.min.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{ asset('template/dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- header -->
  @include('layouts.partials.header')

  <!-- sidebar -->
  @include('layouts.partials.sidebar')

  <!-- content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- content -->
  @include('layouts.partials.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>


<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

<script src="{{ asset('template/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('template/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{ asset('template/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('template/plugins/chart.js/Chart.min.js')}}"></script>
<script src="{{ asset('template/plugins/sparklines/sparkline.js')}}"></script>
<script src="{{ asset('template/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{ asset('template/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<script src="{{ asset('template/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<script src="{{ asset('template/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('template/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{ asset('template/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{ asset('template/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{ asset('template/dist/js/adminlte.js')}}"></script>
<script src="{{ asset('template/dist/js/demo.js')}}"></script>
<script src="{{ asset('template/dist/js/pages/dashboard.js')}}"></script>


</body>
</html>
